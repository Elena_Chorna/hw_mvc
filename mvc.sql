-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 23 2017 г., 12:40
-- Версия сервера: 5.5.53
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mvc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`) VALUES
(1, 'Safari', 'Praesent mauris metus, placerat sit amet nibh eu, sodales iaculis augue. Phasellus porta et risus eget pellentesque. Aenean convallis consequat dolor, ornare auctor quam fringilla vitae. Morbi maximus finibus rutrum. Fusce blandit lectus nibh, non scelerisque velit luctus vel.'),
(2, 'Green wood', 'Morbi maximus finibus rutrum. Fusce blandit lectus nibh, non scelerisque velit luctus vel.Praesent mauris metus, placerat sit amet nibh eu, sodales iaculis augue. Phasellus porta et risus eget pellentesque. Aenean convallis consequat dolor, ornare auctor quam fringilla vitae. ');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `url` varchar(10) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`id`, `year`, `url`, `description`) VALUES
(1, 2015, 'Mountains', 'The print only option is the most affordable route to go. It is just what it sounds like: prints on high quality archival photographic paper without any mounting or framing. One of the benefits of going this route is that you can bring them to your local frame shop to have the matted and framed exactly as you like. Prints are also great if you are in a hurry as they usually arrive in 5-7 days from the time of order.'),
(2, 2016, 'JuneRise', 'This mounting option is by far our most popular format. It is both durable and beautiful. It allows the images to reveal an almost three dimensional quality creating the impression of actually being there. This is because there is almost no glare or reflection such as you find with even the most expensive types of glass. An additional benefit is that the artwork is very durable and can even be cleaned with a soft damp cloth. The artwork also resists fading because it has UV protection. When you order a plaque from us, they arrive ready to hang.'),
(3, 2017, 'SeaView', 'In felis felis, convallis vitae magna et, facilisis venenatis nunc. Vestibulum scelerisque viverra ligula a dictum. Fusce metus sapien, maximus a ligula et, luctus feugiat sem. Suspendisse nec ante pretium ex blandit fermentum a quis enim. Etiam non vestibulum erat. Quisque quis luctus massa. Nullam interdum vulputate ligula, vel vulputate ex.'),
(4, 2017, 'Ocean', 'Nulla et felis cursus, dapibus neque sagittis, imperdiet metus. Praesent mauris metus, placerat sit amet nibh eu, sodales iaculis augue. Phasellus porta et risus eget pellentesque. Aenean convallis consequat dolor, ornare auctor quam fringilla vitae. Morbi maximus finibus rutrum. Fusce blandit lectus nibh, non scelerisque velit luctus vel.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
class Model_Articles extends Model
{
    public function getArticles()
    {
        $pdo = $this::getConnection();
        $result = $pdo->query('SELECT * FROM articles');

        $row = $result -> fetchAll();
        if(empty($row)) {
            return null;
        }
        $articles = array();

        foreach ($row as $key => $value){
            $value = array(
                'id'             => $value['id'],
                'title'          => $value['title'],
                'text'           => $value['text'],
            );
            $articles[] = $value;
        }
        return $articles;
    }

    public function getArticleById($id){
        $pdo = $this::getConnection();
        $query = "SELECT * FROM articles WHERE id=:id";

        $stmt = $pdo->prepare($query);
        $stmt -> bindValue(':id', $id);
        $stmt -> execute();
        $row = $stmt->fetch();
        if (empty($row)) {
            return null;
        }

        $publication = array(
            'id'             => $row['id'],
            'title'          => $row['title'],
            'text'           => $row['text'],
            );
        return $publication;
    }
}
<?php
class Model_Portfolio extends Model
{
    public function getWorks()
    {
        $db = $this::getConnection();
        $result = $db->query('SELECT * FROM portfolio');

        $row = $result -> fetchAll();
        if(empty($row)) {
            return null;
        }
        $works = array();

        foreach ($row as $key => $value){
            $value = array(
                'id'            => $value['id'],
                'year'          => $value['year'],
                'url'           => $value['url'],
                'description'   => $value['description']
            );
            $works[] = $value;
        }
        return $works;
    }
}
<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Portfolio page:</h1>
                <h3>Our works</h3>

            </div>
        </div>
        <div class="row">
            <?php foreach ($data as $work): ?>
            <div class="col-md-6">
                <h4>№<?= $work['id']; ?>: <?= $work['url']; ?></h4>
                <h6>Year: <?= $work['year']; ?></h6>
                <p><?= $work['description']; ?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
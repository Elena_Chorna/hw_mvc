<!DOCTYPE html>
<html>
<head>
    <title>Home Work 12</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href=<?php ROOT; ?>'/application/template/css/bootstrap.min.css'>
    <link rel='stylesheet' href=<?php ROOT; ?>'/application/template/css/style.css'>
</head>
<body>
<header class="header">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">My site</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="/">Home</a></li>
                            <li class="active"><a href="/portfolio">Portfolio</a></li>
                            <li class="active"><a href="/articles">Articles</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
</header>
<?php include 'application/views/'.$content_view;?>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <a class="" href="index.php">My site</a>
            </div>
            <div class="col-sm-12 col-md-8">
                (c) 2017
            </div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php ROOT; ?>/application/template/js/bootstrap.min.js"></script>
</body>
</html>
<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Articles page:</h1>
            </div>
        </div>
        <div class="row">
            <?php foreach ($data as $article): ?>
            <div class="col-md-6">
                <h4><a href="articles/show/<?= $article['id']; ?>"><?= $article['title']; ?></a></h4>
                <p><?= $article['text']; ?></p>
                <a class="read-more" href="articles/show/<?= $article['id']; ?>">Read more <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Articles page:</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4><?= $data['title']; ?></h4>
                <p><?= $data['text']; ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="/articles"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> All Articles </a>
            </div>
        </div>
    </div>
</section>
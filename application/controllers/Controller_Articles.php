<?php

class Controller_Articles extends Controller
{

    public function action_index()
    {
        $articlesModel = new Model_Articles();
        $data = $articlesModel->getArticles();

        $this->view->generate('articles_view.php','template_view.php', $data);
    }

    public function action_show()
    {
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $single_link = '';
        if(!empty($routes[3])){
            $single_link = $routes[3];
        }else{
            Route::ErrorPage404();
        }

        $articlesModel = new Model_Articles();
        $data = $articlesModel->getArticleById($single_link);
        if(!empty($data)){
            $this->view->generate('article_view.php','template_view.php', $data);
        }else{
            Route::ErrorPage404();
        }
    }
}
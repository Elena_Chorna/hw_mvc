<?php
class Controller_Main extends Controller
{

    public function action_index()
    {
        $this->view->generate('main_view.php','template_view.php');
    }

    public function action_aboutUs(){
        $this->view->generate('aboutUs_view.php','template_view.php');
    }
}
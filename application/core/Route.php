<?php
 class Route{
     public static function start(){
         $controller_name = 'Main';
         $action_name = 'index';
         $routes = explode('/', $_SERVER['REQUEST_URI']);

         //get controller name
         if(!empty($routes[1])){
             $controller_name = $routes[1];
         }
         if(!empty($routes[2])){
             $action_name = $routes[2];
         }

         $model_name = 'Model_' . $controller_name;
         //Make a string's first character uppercase
         $controller_name = ucfirst(strtolower($controller_name));
         $controller_name = 'Controller_' . $controller_name;

         $action_name = 'action_' . $action_name;

         $model_path = 'application/models/' . $model_name . '.php';
         if(file_exists($model_path)){
             include_once $model_path;
         }

         $controller_file = $controller_name.'.php';
         $controller_path = 'application/controllers/' . $controller_file;

         if(file_exists($controller_path)){
             include_once $controller_path;
         }else{
             Route::ErrorPage404();
         }

         $controller = new $controller_name;

         if(method_exists($controller, $action_name)){
             $controller->$action_name();
         }else{
             Route::ErrorPage404();
         }
     }
     public static function ErrorPage404(){
         die('404');
     }
 }
<?php
class Model
{
    public static function getConnection()
    {
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=mvc','root','');
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $pdo->exec("SET NAMES 'utf8'");
            return $pdo;

        }catch(PDOException $e){
            echo "Не получилось подключиться к Базе Данных.<br>";
            echo $e->getMessage();
            exit();
        }
    }

}